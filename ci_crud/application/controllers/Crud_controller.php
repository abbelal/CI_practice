<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_controller extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('crud_model');
    }
    
    public function index()
    {	
        $data['users'] = $this->crud_model->get_data();
        $this->load->view('crud_view', $data);
    }

    public function create()
    {
        $this->crud_model->insert_data($_POST);
        redirect('crud_controller');
    }
	
}

?>